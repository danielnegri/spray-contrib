package binarycamp.spray.oauth2.common.server

import akka.actor.ActorSystem
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import binarycamp.spray.oauth2.common.{ AccessToken, TestRedis }
import binarycamp.spray.oauth2.server.ScalaRedisTokenService.Settings
import binarycamp.spray.oauth2.server.TokenService.{ ScopedUserId, TokenRevoked }
import binarycamp.spray.oauth2.server.{ ScalaRedisTokenService, TokenGenerator }
import com.redis.RedisClient
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ BeforeAndAfterAll, FlatSpecLike, Matchers }
import spray.util.Utils

import scala.concurrent.duration._

class ScalaRedisTokenServiceSpec(_system: ActorSystem) extends TestKit(_system)
    with ImplicitSender
    with FlatSpecLike
    with Matchers
    with BeforeAndAfterAll
    with ScalaFutures {

  implicit val ec = system.dispatcher
  implicit val timeout = Timeout(1.second)

  val settings = Settings(TestRedis.host, TestRedis.port, "test-redis-token-service", "test-prefix", 5, 5.seconds)
  val client = RedisClient(settings.host, settings.port, settings.actorName)
  val service = ScalaRedisTokenService(client, TokenGenerator.default, settings)

  def this() = this(ActorSystem(Utils.actorSystemNameFrom(ScalaRedisTokenServiceSpec.getClass)))

  override protected def afterAll() = TestKit.shutdownActorSystem(system)

  val email1 = "user@test.com"
  val email2 = "user2@test.com"
  val email3 = "user3@test.com"
  var token1: AccessToken = null
  var token2: AccessToken = null
  var token3: AccessToken = null
  var token4: AccessToken = null

  def testIssuing(email: String): AccessToken = {
    val result = service.issueToken(email, Set(), false)
    whenReady(result) { res ⇒
      res.isDefined should be(true)
      client.smembers[String](service.userToKey(email)).futureValue.toList.size should be > 0
      client.get[String](service.tokenToKey(res.get.accessToken)).futureValue.isDefined should be(true)
      res.get.accessToken
    }
  }

  "ScalaRedisTokenService" should "issue token" in {
    token1 = testIssuing(email1)
    token2 = testIssuing(email2)
  }

  it should "verify token" in {
    service.verifyToken(token1).futureValue should equal(Some(ScopedUserId(email1, Set())))
    service.verifyToken(token2).futureValue should equal(Some(ScopedUserId(email2, Set())))
  }

  it should "correctly revoke token" in {
    val result = service.revokeToken(token1)
    whenReady(result) { res ⇒
      res should be(TokenRevoked)
      client.smembers[String](service.userToKey(email1)).futureValue.toList.size should be(0)
      client.get[String](service.tokenToKey(token1)).futureValue.isDefined should be(false)
    }
  }

  it should "correctly revoke token by userId" in {
    val result = service.revokeToken(email2)
    whenReady(result) { res ⇒
      res should be(TokenRevoked)
      client.smembers[String](service.userToKey(email2)).futureValue.toList.size should be(0)
      client.get[String](service.tokenToKey(token2)).futureValue.isDefined should be(false)
    }
  }

  it should "correctly issue multiple tokens to single user" in {
    token3 = testIssuing(email3)
    token4 = testIssuing(email3)

    val result = service.revokeToken(token4)
    whenReady(result) { res ⇒
      res should be(TokenRevoked)
      client.smembers[String](service.userToKey(email3)).futureValue.toList.size should be(1)
      client.get[String](service.tokenToKey(token3)).futureValue.isDefined should be(true)
      client.get[String](service.tokenToKey(token4)).futureValue.isDefined should be(false)
    }

    val result2 = service.revokeToken(token3)
    whenReady(result2) { res ⇒
      res should be(TokenRevoked)
      client.smembers[String](service.userToKey(email3)).futureValue.toList.size should be(0)
      client.get[String](service.tokenToKey(token3)).futureValue.isDefined should be(false)
      client.get[String](service.tokenToKey(token4)).futureValue.isDefined should be(false)
    }
  }
}

object ScalaRedisTokenServiceSpec
