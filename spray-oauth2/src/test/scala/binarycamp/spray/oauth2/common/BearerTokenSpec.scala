package binarycamp.spray.oauth2.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class BearerTokenSpec extends FlatSpec {
  val t1 = BearerToken("token")
  val t2 = AccessToken("Bearer", "token", None)
  val t3 = AccessToken("OtherTokenType", "token", None)
  val t4 = AccessToken("Bearer", "token", Some(Map.empty[String, String]))
  val t5 = AccessToken("Bearer", "token", Some(Map("a" -> "b")))
  val t6 = AccessToken("OtherTokenType", "token", Some(Map.empty[String, String]))
  val t7 = AccessToken("OtherTokenType", "token", Some(Map("a" -> "b")))

  "BearerToken" should "equal to an AccessToken with tokenType=Bearer, no additional attributes and the same token" in {
    t1 should be(t2)
    t1 should not be (t3)
    t1 should not be (t4)
    t1 should not be (t5)
    t1 should not be (t6)
    t1 should not be (t7)
  }

  it should "extract only from AccessTokens with tokenType=Bearer and no additional attributes" in {
    BearerToken.unapply(t1) should be(Some("token"))
    BearerToken.unapply(t2) should be(Some("token"))
    BearerToken.unapply(t3) should be(None)
    BearerToken.unapply(t4) should be(None)
    BearerToken.unapply(t5) should be(None)
    BearerToken.unapply(t6) should be(None)
    BearerToken.unapply(t7) should be(None)
  }
}
