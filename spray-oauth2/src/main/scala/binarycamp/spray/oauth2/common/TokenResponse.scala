package binarycamp.spray.oauth2.common

case class TokenResponse(accessToken: AccessToken, refreshToken: Option[String], expiresIn: Long, scopes: Option[Set[String]])

object TokenResponse {
  def apply(accessToken: AccessToken, expiration: Long, scopes: Set[String]): TokenResponse =
    TokenResponse(accessToken, None, expiration, Some(scopes))

  def apply(accessToken: AccessToken, refreshToken: String, expiration: Long, scopes: Set[String]): TokenResponse =
    TokenResponse(accessToken, Some(refreshToken), expiration, Some(scopes))
}
