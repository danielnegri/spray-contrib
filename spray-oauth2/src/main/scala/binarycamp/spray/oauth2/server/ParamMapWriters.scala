package binarycamp.spray.oauth2.server

import binarycamp.spray.common.ParamMapWriter
import binarycamp.spray.data.MarshallableError
import binarycamp.spray.oauth2.common.TokenResponse
import binarycamp.spray.oauth2.server.RequestResponseElements._
import binarycamp.spray.pimp._

trait ParamMapWriters {
  implicit val TokenParamMapWriter = new ParamMapWriter[TokenResponse] {
    override def apply(token: TokenResponse): Map[String, String] =
      Map(AccessToken -> token.accessToken.token, TokenType -> token.accessToken.tokenType,
        ExpiresIn -> token.expiresIn.toString()) +? (ScopeParam -> token.scopes.map(_.mkString(" "))) ++
        ParamMapWriter.write(token.accessToken.additionalAttributes)
  }

  implicit val errorParamMapWriter = new ParamMapWriter[MarshallableError] {
    override def apply(error: MarshallableError): Map[String, String] =
      Map("error" -> error.code, "error_description" -> error.message)
  }
}

object ParamMapWriters extends ParamMapWriters
