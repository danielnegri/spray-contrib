package binarycamp.spray.oauth2

import binarycamp.spray.data.MarshallableError
import binarycamp.spray.login.UserInfo
import spray.http.Uri.Path
import spray.routing._
import spray.routing.authentication.ContextAuthenticator

package object server {
  type ClientAuthenticator = ContextAuthenticator[Client]

  type ApprovalPageRoute[U] = (UserInfo[U], Client, Set[Scope], Path, Path) ⇒ Route
  type ErrorPageRoute = MarshallableError ⇒ Route
}
