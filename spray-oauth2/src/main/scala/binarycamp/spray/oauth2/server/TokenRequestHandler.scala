package binarycamp.spray.oauth2.server

import spray.routing.{ Directive1, Route }

sealed trait TokenRequestHandler extends RequestHandler {
  def optionalClient(params: RequestParameters): Directive1[Option[Client]]
}

trait AccessTokenRequestHandler extends TokenRequestHandler {
  def canHandle(params: RequestParameters): Boolean
  def handle(client: Client, scopes: Set[String], params: RequestParameters): Route
}

trait RefreshRequestHandler extends TokenRequestHandler {
  def handle(client: Client, scopes: Option[Set[String]], params: RequestParameters): Route
}
