package binarycamp.spray.oauth2.server

import binarycamp.spray.oauth2.common.{ AccessToken, TokenResponse }
import scala.concurrent.{ ExecutionContext, Future }

trait TokenService {
  import binarycamp.spray.oauth2.server.TokenService._

  def issueToken(userId: String, scopes: Set[String], refreshToken: Boolean)(implicit ec: ExecutionContext): Future[Option[TokenResponse]]
  def refreshToken(refreshToken: String, scopes: Option[Set[String]])(implicit ec: ExecutionContext): Future[Option[TokenResponse]]
  def verifyToken(accessToken: AccessToken)(implicit ec: ExecutionContext): Future[Option[ScopedUserId]]
  def revokeToken(accessToken: AccessToken)(implicit ec: ExecutionContext): Future[RevocationResponse]
  def revokeToken(userId: String)(implicit ec: ExecutionContext): Future[RevocationResponse]
}

object TokenService {
  final case class ScopedUserId(id: String, scopes: Set[String])

  sealed trait RevocationResponse
  object TokenRevoked extends RevocationResponse
  case class TokenRevocationFailed(error: String) extends RevocationResponse

}
