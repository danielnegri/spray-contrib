package binarycamp.spray.examples.session

import akka.actor.ActorSystem
import binarycamp.spray.session._
import scala.language.postfixOps
import scala.xml.NodeSeq
import spray.http.MediaTypes._
import spray.httpx.marshalling.Marshaller
import spray.routing.{ RejectionHandler, SimpleRoutingApp }

object Main extends App with SimpleRoutingApp with SessionDirectives {
  implicit val system = ActorSystem("example")

  implicit val marshaller = Marshaller.delegate[NodeSeq, String](`text/html`)(_.toString)

  val handler = RejectionHandler {
    case MissingSessionAttributeRejection(name) :: _ ⇒ complete(page(s"attribute $name is missing!"))
  }

  def page(result: String): NodeSeq =
    <html>
      <body>
        <div>{ result }</div>
        <div><a href="/">back</a></div>
      </body>
    </html>

  startServer("localhost", 8080) {
    get {
      pathEndOrSingleSlash {
        complete {
          <html>
            <body>
              <ul>
                <li><a href="/set-attr1">Set attr1="abc"</a></li>
                <li><a href="/set-attr2">Set attr2=100</a></li>
                <li><a href="/get-attr1">Get attr1 (must be set first!)</a></li>
                <li><a href="/get-attr2">Get attr2 (must be set first!)</a></li>
                <li><a href="/get-opt-attr1">Get optional attr1</a></li>
                <li><a href="/get-opt-attr2">Get optional attr2</a></li>
                <li><a href="/remove-attr1">Remove attr1</a></li>
                <li><a href="/remove">Remove session</a></li>
              </ul>
            </body>
          </html>
        }
      } ~ path("set-attr1") {
        setSession("attr1" set "abc") {
          complete(page("done"))
        }
      } ~ path("set-attr2") {
        setSession("attr2" set 100) {
          complete(page("done"))
        }
      } ~ path("get-attr1") {
        handleRejections(handler) {
          session("attr1") { value ⇒
            complete(page(s"attr1=$value"))
          }
        }
      } ~ path("get-attr2") {
        handleRejections(handler) {
          session("attr2".as[Int]) { value ⇒
            complete(page(s"attr2=$value"))
          }
        }
      } ~ path("get-opt-attr1") {
        session("attr1"?) { value ⇒
          complete(page(s"attr1=$value"))
        }
      } ~ path("get-opt-attr2") {
        session("attr2".as[Int]?) { value ⇒
          complete(page(s"attr2=$value"))
        }
      } ~ path("remove-attr1") {
        setSession("attr1" remove) {
          complete(page("done"))
        }
      } ~ path("remove") {
        removeSession() {
          complete(page("done"))
        }
      }
    }
  }

  sys.addShutdownHook(system.shutdown())
}
