package binarycamp.spray.data

import org.parboiled2.ParseError
import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import Order._

class SortParserSpec extends FlatSpec {
  def parseSort(input: String): Sort = new SortParser(input).sort.run().get

  "SortParser" should "parse a1" in {
    parseSort("a1") should be(Sort("a1" -> None))
  }

  it should "parse a1:asc" in {
    parseSort("a1:asc") should be(Sort("a1" -> Some(Asc)))
  }

  it should "parse a1:desc" in {
    parseSort("a1:desc") should be(Sort("a1" -> Some(Desc)))
  }

  it should "parse a1:asc,a2:desc" in {
    parseSort("a1:asc,a2:desc") should be(Sort("a1" -> Some(Asc), "a2" -> Some(Desc)))
  }

  it should "parse a1:Asc,a2:DESC" in {
    parseSort("a1:Asc,a2:DESC") should be(Sort("a1" -> Some(Asc), "a2" -> Some(Desc)))
  }

  it should "produce ParsingException for an empty String" in {
    intercept[ParseError] {
      parseSort("")
    }
  }

  it should "produce ParsingException for a1:" in {
    intercept[ParseError] {
      parseSort("a1:")
    }
  }

  it should "produce ParsingException for a1:x" in {
    intercept[ParseError] {
      parseSort("a1:x")
    }
  }

  it should "produce ParsingException for a1," in {
    intercept[ParseError] {
      parseSort("a1,")
    }
  }

  it should "produce ParsingException for a1:asc," in {
    intercept[ParseError] {
      parseSort("a1:asc,")
    }
  }
}
