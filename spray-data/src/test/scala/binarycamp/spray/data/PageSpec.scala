package binarycamp.spray.data

import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class PageSpec extends FlatSpec with PageBehaviours {
  val empty = Seq.empty[String]
  val items = Seq("1", "2", "3")
  val sort = Sort("p1" -> Some(Order.Asc), "p2" -> Some(Order.Desc), "p3" -> None)
  val filter = Filter("expression")

  "Page(items=empty)" should behave like
    emptyPage(Page(empty), 0, Page.defaultSize, 0)

  "Page(items=empty, sort)" should behave like
    emptyPage(Page(empty, sort), 0, Page.defaultSize, 0, sort = Some(sort))

  "Page(items=empty, filter)" should behave like
    emptyPage(Page(empty, filter), 0, Page.defaultSize, 0, filter = Some(filter))

  "Page(items=empty, sort, filter)" should behave like
    emptyPage(Page(empty, sort, filter), 0, Page.defaultSize, 0, sort = Some(sort), filter = Some(filter))

  "Page(items=[1,2,3])" should behave like
    page(Page(items), items, 0, 3, 3)

  "Page(items=[1,2,3], sort)" should behave like
    page(Page(items, sort), items, 0, 3, 3, sort = Some(sort))

  "Page(items=[1,2,3], filter)" should behave like
    page(Page(items, filter), items, 0, 3, 3, filter = Some(filter))

  "Page(items=[1,2,3], sort, filter)" should behave like
    page(Page(items, sort, filter), items, 0, 3, 3, sort = Some(sort), filter = Some(filter))

  "Page(items=empty, index=-1, size=3)" should behave like
    pageWithInvalidIndex(Page(empty, -1, 3))

  "Page(items=empty, index=-1, size=3, sort)" should behave like
    pageWithInvalidIndex(Page(empty, -1, 3, sort))

  "Page(items=empty, index=-1, size=3, filter)" should behave like
    pageWithInvalidIndex(Page(empty, -1, 3, filter))

  "Page(items=empty, index=-1, size=3, sort, filter)" should behave like
    pageWithInvalidIndex(Page(empty, -1, 3, sort, filter))

  "Page(items=empty, index=0, size=0)" should behave like
    pageWithInvalidSize(Page(empty, 0, 0))

  "Page(items=empty, index=0, size=0, sort)" should behave like
    pageWithInvalidSize(Page(empty, 0, 0, sort))

  "Page(items=empty, index=0, size=0, filter)" should behave like
    pageWithInvalidSize(Page(empty, 0, 0, filter))

  "Page(items=empty, index=0, size=0, sort, fiter)" should behave like
    pageWithInvalidSize(Page(empty, 0, 0, sort, filter))

  "Page(items=empty, index=0, size=3)" should behave like
    emptyPage(Page(empty, 0, 3), 0, 3, 0)

  "Page(items=empty, index=0, size=3, sort)" should behave like
    emptyPage(Page(empty, 0, 3, sort), 0, 3, 0, sort = Some(sort))

  "Page(items=empty, index=0, size=3, filter)" should behave like
    emptyPage(Page(empty, 0, 3, filter), 0, 3, 0, filter = Some(filter))

  "Page(items=empty, index=0, size=3, sort, filter)" should behave like
    emptyPage(Page(empty, 0, 3, sort, filter), 0, 3, 0, sort = Some(sort), filter = Some(filter))

  "Page(items=empty, index=1, size=3)" should behave like
    emptyPage(Page(empty, 1, 3), 1, 3, 3, prev = true)

  "Page(items=empty, index=1, size=3, sort)" should behave like
    emptyPage(Page(empty, 1, 3, sort), 1, 3, 3, prev = true, sort = Some(sort))

  "Page(items=empty, index=1, size=3, filter)" should behave like
    emptyPage(Page(empty, 1, 3, filter), 1, 3, 3, prev = true, filter = Some(filter))

  "Page(items=empty, index=1, size=3, sort, filter)" should behave like
    emptyPage(Page(empty, 1, 3, sort, filter), 1, 3, 3, prev = true, sort = Some(sort), filter = Some(filter))

  "Page(items=[1,2,3], index=-1, size=3)" should behave like
    pageWithInvalidIndex(Page(items, -1, 3))

  "Page(items=[1,2,3], index=-1, size=3, sort)" should behave like
    pageWithInvalidIndex(Page(items, -1, 3, sort))

  "Page(items=[1,2,3], index=-1, size=3, filter)" should behave like
    pageWithInvalidIndex(Page(items, -1, 3, filter))

  "Page(items=[1,2,3], index=-1, size=3, sort, filter)" should behave like
    pageWithInvalidIndex(Page(items, -1, 3, sort, filter))

  "Page(items=[1,2,3], index=0, size=0)" should behave like
    pageWithInvalidSize(Page(items, 0, 0))

  "Page(items=[1,2,3], index=0, size=0, sort)" should behave like
    pageWithInvalidSize(Page(items, 0, 0, sort))

  "Page(items=[1,2,3], index=0, size=0, filter)" should behave like
    pageWithInvalidSize(Page(items, 0, 0, filter))

  "Page(items=[1,2,3], index=0, size=0, sort, filter)" should behave like
    pageWithInvalidSize(Page(items, 0, 0, sort, filter))

  "Page(items=[1,2,3], index=0, size=3)" should behave like
    pageWithSize(Page(items, 0, 3), items, 0, 3, 3)

  "Page(items=[1,2,3], index=0, size=3, sort)" should behave like
    pageWithSize(Page(items, 0, 3, sort), items, 0, 3, 3, sort = Some(sort))

  "Page(items=[1,2,3], index=0, size=3, filter)" should behave like
    pageWithSize(Page(items, 0, 3, filter), items, 0, 3, 3, filter = Some(filter))

  "Page(items=[1,2,3], index=0, size=3, sort, filter)" should behave like
    pageWithSize(Page(items, 0, 3, sort, filter), items, 0, 3, 3, sort = Some(sort), filter = Some(filter))

  "Page(items=[1,2,3], index=0, size=2)" should behave like
    pageWithSize(Page(items, 0, 2), items, 0, 2, Int.MaxValue, next = true)

  "Page(items=[1,2,3], index=0, size=2, sort)" should behave like
    pageWithSize(Page(items, 0, 2, sort), items, 0, 2, Int.MaxValue, next = true, sort = Some(sort))

  "Page(items=[1,2,3], index=0, size=2, filter)" should behave like
    pageWithSize(Page(items, 0, 2, filter), items, 0, 2, Int.MaxValue, next = true, filter = Some(filter))

  "Page(items=[1,2,3], index=0, size=2, sort, filter)" should behave like
    pageWithSize(Page(items, 0, 2, sort, filter), items, 0, 2, Int.MaxValue, next = true, sort = Some(sort),
      filter = Some(filter))

  "Page(items=[1,2,3], index=0, size=4)" should behave like
    pageWithSize(Page(items, 0, 4), items, 0, 4, 3, next = true)

  "Page(items=[1,2,3], index=0, size=4, sort)" should behave like
    pageWithSize(Page(items, 0, 4, sort), items, 0, 4, 3, next = true, sort = Some(sort))

  "Page(items=[1,2,3], index=0, size=4, filter)" should behave like
    pageWithSize(Page(items, 0, 4, filter), items, 0, 4, 3, next = true, filter = Some(filter))

  "Page(items=[1,2,3], index=0, size=4, sort, filter)" should behave like
    pageWithSize(Page(items, 0, 4, sort, filter), items, 0, 4, 3, next = true, sort = Some(sort), filter = Some(filter))

  "Page(items=[1,2,3], index=1, size=0)" should behave like
    pageWithInvalidSize(Page(items, 1, 0))

  "Page(items=[1,2,3], index=1, size=0, sort)" should behave like
    pageWithInvalidSize(Page(items, 1, 0, sort))

  "Page(items=[1,2,3], index=1, size=0, filter)" should behave like
    pageWithInvalidSize(Page(items, 1, 0, filter))

  "Page(items=[1,2,3], index=1, size=0, sort, filter)" should behave like
    pageWithInvalidSize(Page(items, 1, 0, sort, filter))

  "Page(items=[1,2,3], index=1, size=3)" should behave like
    pageWithSize(Page(items, 1, 3), items, 1, 3, 6, prev = true)

  "Page(items=[1,2,3], index=1, size=3, sort)" should behave like
    pageWithSize(Page(items, 1, 3, sort), items, 1, 3, 6, prev = true, sort = Some(sort))

  "Page(items=[1,2,3], index=1, size=3, filter)" should behave like
    pageWithSize(Page(items, 1, 3, filter), items, 1, 3, 6, prev = true, filter = Some(filter))

  "Page(items=[1,2,3], index=1, size=3, sort, filter)" should behave like
    pageWithSize(Page(items, 1, 3, sort, filter), items, 1, 3, 6, prev = true, sort = Some(sort), filter = Some(filter))

  "Page(items=[1,2,3], index=1, size=2)" should behave like
    pageWithSize(Page(items, 1, 2), items, 1, 2, Int.MaxValue, prev = true, next = true)

  "Page(items=[1,2,3], index=1, size=2, sort)" should behave like
    pageWithSize(Page(items, 1, 2, sort), items, 1, 2, Int.MaxValue, prev = true, next = true, sort = Some(sort))

  "Page(items=[1,2,3], index=1, size=2, filter)" should behave like
    pageWithSize(Page(items, 1, 2, filter), items, 1, 2, Int.MaxValue, prev = true, next = true, filter = Some(filter))

  "Page(items=[1,2,3], index=1, size=2, sort, filter)" should behave like
    pageWithSize(Page(items, 1, 2, sort, filter), items, 1, 2, Int.MaxValue, prev = true, next = true, sort = Some(sort),
      filter = Some(filter))

  "Page(items=[1,2,3], index=1, size=4)" should behave like
    pageWithSize(Page(items, 1, 4), items, 1, 4, 7, prev = true, next = true)

  "Page(items=[1,2,3], index=1, size=4, sort)" should behave like
    pageWithSize(Page(items, 1, 4, sort), items, 1, 4, 7, prev = true, next = true, sort = Some(sort))

  "Page(items=[1,2,3], index=1, size=4, filter)" should behave like
    pageWithSize(Page(items, 1, 4, filter), items, 1, 4, 7, prev = true, next = true, filter = Some(filter))

  "Page(items=[1,2,3], index=1, size=4, sort, filter)" should behave like
    pageWithSize(Page(items, 1, 4, sort, filter), items, 1, 4, 7, prev = true, next = true, sort = Some(sort), filter = Some(filter))

  "Page(items=empty, index=-1, size=3, count=0)" should behave like
    pageWithInvalidIndex(Page(empty, -1, 3, 0))

  "Page(items=empty, index=-1, size=3, count=0, sort)" should behave like
    pageWithInvalidIndex(Page(empty, -1, 3, 0, sort))

  "Page(items=empty, index=-1, size=3, count=0, filter)" should behave like
    pageWithInvalidIndex(Page(empty, -1, 3, 0, filter))

  "Page(items=empty, index=-1, size=3, count=0, sort, filter)" should behave like
    pageWithInvalidIndex(Page(empty, -1, 3, 0, sort, filter))

  "Page(items=empty, index=0, size=0, count=0)" should behave like
    pageWithInvalidSize(Page(empty, 0, 0, 0))

  "Page(items=empty, index=0, size=0, count=0, sort)" should behave like
    pageWithInvalidSize(Page(empty, 0, 0, 0, sort))

  "Page(items=empty, index=0, size=0, count=0, filter)" should behave like
    pageWithInvalidSize(Page(empty, 0, 0, 0, filter))

  "Page(items=empty, index=0, size=0, count=0, sort, filter)" should behave like
    pageWithInvalidSize(Page(empty, 0, 0, 0, sort, filter))

  "Page(items=empty, index=0, size=3, count=-1)" should behave like
    pageWithInvalidCount(Page(empty, 0, 3, -1))

  "Page(items=empty, index=0, size=3, count=-1, sort)" should behave like
    pageWithInvalidCount(Page(empty, 0, 3, -1, sort))

  "Page(items=empty, index=0, size=3, count=-1, filter)" should behave like
    pageWithInvalidCount(Page(empty, 0, 3, -1, filter))

  "Page(items=empty, index=0, size=3, count=-1, sort, filter)" should behave like
    pageWithInvalidCount(Page(empty, 0, 3, -1, sort, filter))

  "Page(items=empty, index=0, size=3, count=0)" should behave like
    emptyPage(Page(empty, 0, 3, 0), 0, 3, 0)

  "Page(items=empty, index=0, size=3, count=0, sort)" should behave like
    emptyPage(Page(empty, 0, 3, 0, sort), 0, 3, 0, sort = Some(sort))

  "Page(items=empty, index=0, size=3, count=0, filter)" should behave like
    emptyPage(Page(empty, 0, 3, 0, filter), 0, 3, 0, filter = Some(filter))

  "Page(items=empty, index=0, size=3, count=0, sort, filter)" should behave like
    emptyPage(Page(empty, 0, 3, 0, sort, filter), 0, 3, 0, sort = Some(sort), filter = Some(filter))

  "Page(items=empty, index=0, size=3, count=1)" should behave like
    pageWithInvalidCount(Page(empty, 0, 3, 1))

  "Page(items=empty, index=0, size=3, count=1, sort)" should behave like
    pageWithInvalidCount(Page(empty, 0, 3, 1, sort))

  "Page(items=empty, index=0, size=3, count=1, filter)" should behave like
    pageWithInvalidCount(Page(empty, 0, 3, 1, filter))

  "Page(items=empty, index=0, size=3, count=1, sort, filter)" should behave like
    pageWithInvalidCount(Page(empty, 0, 3, 1, sort, filter))

  "Page(items=empty, index=1, size=0, count=0)" should behave like
    pageWithInvalidSize(Page(empty, 1, 0, 0))

  "Page(items=empty, index=1, size=3, count=-1)" should behave like
    pageWithInvalidCount(Page(empty, 1, 3, -1))

  "Page(items=empty, index=1, size=3, count=3)" should behave like
    emptyPage(Page(empty, 1, 3, 3), 1, 3, 3, prev = true)

  "Page(items=empty, index=1, size=3, count=3, sort)" should behave like
    emptyPage(Page(empty, 1, 3, 3, sort), 1, 3, 3, prev = true, sort = Some(sort))

  "Page(items=empty, index=1, size=3, count=3, filter)" should behave like
    emptyPage(Page(empty, 1, 3, 3, filter), 1, 3, 3, prev = true, filter = Some(filter))

  "Page(items=empty, index=1, size=3, count=3, sort, filter)" should behave like
    emptyPage(Page(empty, 1, 3, 3, sort, filter), 1, 3, 3, prev = true, sort = Some(sort), filter = Some(filter))

  "Page(items=empty, index=1, size=3, count=4)" should behave like
    pageWithInvalidCount(Page(empty, 1, 3, 4))

  "Page(items=[1,2,3], index=-1, size=3, count=3)" should behave like
    pageWithInvalidIndex(Page(items, -1, 3, 3))

  "Page(items=[1,2,3], index=0, size=0, count=3)" should behave like
    pageWithInvalidSize(Page(items, 0, 0, 3))

  "Page(items=[1,2,3], index=0, size=2, count=3)" should behave like
    pageWithInvalidSize(Page(items, 0, 2, 3))

  "Page(items=[1,2,3], index=0, size=3, count=-1)" should behave like
    pageWithInvalidCount(Page(items, 0, 3, -1))

  "Page(items=[1,2,3], index=0, size=3, count=0)" should behave like
    pageWithInvalidCount(Page(items, 0, 3, 0))

  "Page(items=[1,2,3], index=0, size=3, count=2)" should behave like
    pageWithInvalidCount(Page(items, 0, 3, 2))

  "Page(items=[1,2,3], index=0, size=3, count=3)" should behave like
    page(Page(items, 0, 3, 3), items, 0, 3, 3)

  "Page(items=[1,2,3], index=0, size=3, count=4)" should behave like
    page(Page(items, 0, 3, 4), items, 0, 3, 4, next = true)

  "Page(items=[1,2,3], index=0, size=4, count=2)" should behave like
    pageWithInvalidCount(Page(items, 0, 4, 2))

  "Page(items=[1,2,3], index=0, size=4, count=3)" should behave like
    page(Page(items, 0, 4, 3), items, 0, 4, 3)

  "Page(items=[1,2,3], index=0, size=4, count=4)" should behave like
    pageWithInvalidCount(Page(items, 0, 4, 4))

  "Page(items=[1,2,3], index=1, size=0, count=6)" should behave like
    pageWithInvalidSize(Page(items, 1, 0, 6))

  "Page(items=[1,2,3], index=1, size=2, count=6)" should behave like
    pageWithInvalidSize(Page(items, 1, 2, 6))

  "Page(items=[1,2,3], index=1, size=3, count=5)" should behave like
    pageWithInvalidCount(Page(items, 1, 3, 5))

  "Page(items=[1,2,3], index=1, size=3, count=6)" should behave like
    page(Page(items, 1, 3, 6), items, 1, 3, 6, prev = true)

  "Page(items=[1,2,3], index=1, size=3, count=7)" should behave like
    page(Page(items, 1, 3, 7), items, 1, 3, 7, prev = true, next = true)

  "Page(items=[1,2,3], index=1, size=4, count=6)" should behave like
    pageWithInvalidCount(Page(items, 1, 4, 6))

  "Page(items=[1,2,3], index=1, size=4, count=7)" should behave like
    page(Page(items, 1, 4, 7), items, 1, 4, 7, prev = true)

  "Page(items=[1,2,3], index=1, size=4, count=8)" should behave like
    pageWithInvalidCount(Page(items, 1, 4, 8))
}

trait PageBehaviours {
  this: FlatSpec ⇒

  def page[T](page: ⇒ Page[T], items: Seq[T], index: Int, size: Int, count: Int, prev: Boolean = false,
              next: Boolean = false, sort: Option[Sort] = None, filter: Option[Filter] = None) = {
    val p = page
    it should "create a page with the provided list of items" in {
      p.items should be(items)
    }

    common(p, index, size, count, prev, next, sort, filter)
  }

  def pageWithSize[T](page: ⇒ Page[T], items: Seq[T], index: Int, size: Int, count: Int, prev: Boolean = false,
                      next: Boolean = false, sort: Option[Sort] = None, filter: Option[Filter] = None) = {
    val p = page
    if (size < items.size) {
      it should s"create a page with first $size items when size < items.size" in {
        p.items should be(items.slice(0, size))
      }
    } else {
      it should "create a page with the provided list of items when size == items.size" in {
        p.items should be(items)
      }
    }

    common(p, index, size, count, prev, next, sort, filter)
  }

  def emptyPage[T](page: ⇒ Page[T], index: Int, size: Int, count: Int, prev: Boolean = false, sort: Option[Sort] = None,
                   filter: Option[Filter] = None) = {
    val p = page
    it should "create a page with an empty list of items" in {
      p.items.isEmpty should be(true)
    }

    common(p, index, size, count, prev, false, sort, filter)
  }

  private def common[T](page: Page[T], index: Int, size: Int, count: Int, prev: Boolean,
                        next: Boolean, sort: Option[Sort], filter: Option[Filter]) = {
    it should s"create a page with index $index" in {
      page.index should be(index)
    }

    it should s"create a page with size $size" in {
      page.size should be(size)
    }

    it should s"create a page with count ${if (count == Int.MaxValue) "== infinity" else count}" in {
      page.count should be(count)
    }

    it should (if (prev) "create a page with a previous page" else "create a page with no previous page") in {
      page.hasPrevious should be(prev)
    }

    it should (if (next) "create a page with a next page" else "create a page with no next page") in {
      page.hasPrevious should be(prev)
    }

    it should (if (sort.isDefined) "create a page with sort" else "create a page with no sort") in {
      page.sort should be(sort)
    }

    it should (if (filter.isDefined) "create a page with filter" else "create a page with no filter") in {
      page.filter should be(filter)
    }
  }

  def pageWithInvalidIndex[T](page: ⇒ Page[T]) =
    it should "produce an IllegalArgumentException" in {
      intercept[IllegalArgumentException] {
        page
      }.getMessage should be("requirement failed: Page index must be a non-negative integer")
    }

  def pageWithInvalidSize[T](page: ⇒ Page[T]) =
    it should "produce an IllegalArgumentException" in {
      intercept[IllegalArgumentException] {
        page
      }.getMessage should (
        be("requirement failed: Page size must be a positive integer") or
        be("requirement failed: Page size must be at least the size of items"))
    }

  def pageWithInvalidCount[T](page: ⇒ Page[T]) =
    it should "produce an IllegalArgumentException" in {
      intercept[IllegalArgumentException] {
        page
      }.getMessage should be("requirement failed: Invalid total items count")
    }
}
