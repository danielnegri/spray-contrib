package binarycamp.spray.data

import spray.httpx.unmarshalling._
import spray.routing._
import spray.routing.Directives._
import PageRequestDeserializers._

trait PageDirectives {
  def getWithPageRequest: Directive1[Option[PageRequest]] = PageDirectives._getWithPageRequest
}

object PageDirectives extends PageDirectives {
  private val _getWithPageRequest: Directive1[Option[PageRequest]] = get & pageRequest

  private def pageRequest: Directive1[Option[PageRequest]] =
    parameterMultiMap.flatMap { params ⇒
      PageRequestDeserializers.read(params) match {
        case Right(pageRequest)                   ⇒ provide(pageRequest)
        case Left(MalformedContent(error, cause)) ⇒ reject(MalformedPageRequestRejection(error, cause))
        case Left(error)                          ⇒ throw new IllegalStateException(error.toString)
      }
    }

  case class MalformedPageRequestRejection(errorMsg: String, cause: Option[Throwable] = None) extends Rejection
}
