package binarycamp.spray.data

trait Page[T] {
  def items: Seq[T]

  def index: Int

  def size: Int

  def count: Int

  final def isFirst: Boolean = !hasPrevious

  def hasPrevious: Boolean

  def hasNext: Boolean

  final def isLast: Boolean = !hasNext

  def map[B](f: T ⇒ B): Page[B]

  def sort: Option[Sort]

  def filter: Option[Filter]
}

object Page {
  val defaultSize = 10

  def apply[T](items: Seq[T]): Page[T] =
    apply(items, None, None)

  def apply[T](items: Seq[T], sort: Sort): Page[T] =
    apply(items, Some(sort), None)

  def apply[T](items: Seq[T], filter: Filter): Page[T] =
    apply(items, None, Some(filter))

  def apply[T](items: Seq[T], sort: Sort, filter: Filter): Page[T] =
    apply(items, Some(sort), Some(filter))

  def apply[T](items: Seq[T], sort: Option[Sort], filter: Option[Filter]): Page[T] =
    new PageImpl[T](items, 0, if (items.isEmpty) defaultSize else items.size, items.size, sort, filter)

  def apply[T](items: Seq[T], index: Int, size: Int): Page[T] =
    apply(items, index, size, None, None)

  def apply[T](items: Seq[T], index: Int, size: Int, sort: Sort): Page[T] =
    apply(items, index, size, Some(sort), None)

  def apply[T](items: Seq[T], index: Int, size: Int, filter: Filter): Page[T] =
    apply(items, index, size, None, Some(filter))

  def apply[T](items: Seq[T], index: Int, size: Int, sort: Sort, filter: Filter): Page[T] =
    apply(items, index, size, Some(sort), Some(filter))

  def apply[T](items: Seq[T], index: Int, size: Int, sort: Option[Sort], filter: Option[Filter]): Page[T] = {
    val count = if (items.size > size) Int.MaxValue else index * size + items.size
    new PageImpl[T](items.slice(0, size), index, size, count, sort, filter)
  }

  def apply[T](items: Seq[T], index: Int, size: Int, count: Int): Page[T] =
    apply(items, index, size, count, None, None)

  def apply[T](items: Seq[T], index: Int, size: Int, count: Int, sort: Sort): Page[T] =
    apply(items, index, size, count, Some(sort), None)

  def apply[T](items: Seq[T], index: Int, size: Int, count: Int, filter: Filter): Page[T] =
    apply(items, index, size, count, None, Some(filter))

  def apply[T](items: Seq[T], index: Int, size: Int, count: Int, sort: Sort, filter: Filter): Page[T] =
    apply(items, index, size, count, Some(sort), Some(filter))

  def apply[T](items: Seq[T], index: Int, size: Int, count: Int, sort: Option[Sort], filter: Option[Filter]): Page[T] =
    new PageImpl[T](items, index, size, count, sort, filter)
}

private[data] case class PageImpl[T](items: Seq[T], index: Int, size: Int, count: Int, sort: Option[Sort],
                                     filter: Option[Filter]) extends Page[T] {
  require(index >= 0, "Page index must be a non-negative integer")
  require(size > 0, "Page size must be a positive integer")
  require(size >= items.size, "Page size must be at least the size of items")

  if (items.isEmpty)
    require(0 <= count && count <= index * size, "Invalid total items count")
  else if (items.size < size)
    require(index * size + items.size == count, "Invalid total items count")
  else
    require(index * size + items.size <= count, "Invalid total items count")

  override val hasPrevious: Boolean = index > 0

  override val hasNext: Boolean = (index + 1) * size < count

  override def map[B](f: T ⇒ B): Page[B] = new PageImpl[B](items.map(f), index, size, count, sort, filter)
}
