package binarycamp.spray.common

import spray.http.HttpHeaders.`Accept-Language`
import spray.routing._
import spray.util._

trait LanguageDirectives {
  def completeWithLanguage[T](t: ⇒ T)(implicit m: LocalizableMarshaller[T]): StandardRoute =
    new StandardRoute {
      override def apply(ctx: RequestContext): Unit = ctx.complete(t)(m(languages(ctx)))
    }

  private def languages(ctx: RequestContext) =
    ctx.request.headers.findByType[`Accept-Language`].map(header ⇒ header.languages).getOrElse(Seq())
}
