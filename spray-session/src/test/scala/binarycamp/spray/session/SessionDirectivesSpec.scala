package binarycamp.spray.session

import org.scalatest.FunSpec
import org.scalatest.Matchers._
import scala.language.postfixOps
import spray.http._
import spray.http.HttpHeaders._
import spray.httpx.unmarshalling._
import spray.routing.Directives
import spray.testkit.ScalatestRouteTest

class SessionDirectivesSpec extends FunSpec with ScalatestRouteTest with Directives with SessionDirectives {
  trait PlainBaker {
    implicit val baker = CookieBaker(PlainCoder, "session")
  }

  trait AesBaker {
    val coder = new JavaxCryptoCoder("AES", "AES/ECB/PKCS5Padding", "secret", "PBKDF2WithHmacSHA1", 1, 128)
    implicit val baker = CookieBaker(coder, "session")
  }

  trait CookieBuilding {
    def baker: CookieBaker

    def createCookie(t: (String, String)*) = Cookie(baker.encode(Map(t: _*)).right.get)

    implicit class Converter(val name: String) {
      def <<[A](value: A)(implicit s: ToStringSerializer[A]) = (name, s(value))
    }
  }

  implicit val LongUnmarshaller = new Unmarshaller[Long] {
    def apply(entity: HttpEntity): Deserialized[Long] = {
      val str = entity.asString
      try { Right(str.toLong) } catch { case e: NumberFormatException ⇒ Left(MalformedContent(s"$str' is not a Long")) }
    }
  }

  describe("The session directive") {
    it("should reject request if the session cookie is not present") {
      new PlainBaker {
        Get() ~> {
          session("attr") { complete(_) }
        } ~> check {
          rejections match {
            case MissingSessionAttributeRejection(_) :: _ ⇒
            case _                                        ⇒ fail()
          }
        }
      }
    }

    it("should reject request if the session cookie is malformed") {
      new AesBaker {
        Get() ~> addHeader(Cookie(HttpCookie(baker.name, "malformedsession"))) ~> {
          session("attr") { complete(_) }
        } ~> check {
          rejections match {
            case MalformedSessionRejection(_, _) :: _ ⇒
            case _                                    ⇒ fail()
          }
        }
      }
    }

    it("should extract a String attribute") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr" << "value")) ~> {
          session("attr") { complete(_) }
        } ~> check {
          responseAs[String] should be("value")
        }
      }
    }

    it("should extract a String tuple") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr" << "value", "attr2" << "value2")) ~> {
          session("attr", "attr2") { (attr, attr2) ⇒ complete(s"$attr, $attr2") }
        } ~> check {
          responseAs[String] should be("value, value2")
        }
      }
    }

    it("should extract an Int attribute") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr" << 1)) ~> {
          session("attr") { complete(_) }
        } ~> check {
          responseAs[Long] should be(1)
        }
      }
    }

    it("should extract a Long attribute") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr" << 1L)) ~> {
          session("attr") { complete(_) }
        } ~> check {
          responseAs[Long] should be(1L)
        }
      }
    }

    it("should extract a Uri attribute") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr" << Uri("http://localhost"))) ~> {
          session("attr".as[Uri]) { attr ⇒ complete(attr.toString) }
        } ~> check {
          responseAs[String] should be("http://localhost")
        }
      }
    }

    it("should extract Some(value) if an optional attribute is present") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr" << "value")) ~> {
          session("attr" ?) { attr ⇒ complete(attr.toString) }
        } ~> check {
          responseAs[String] should be("Some(value)")
        }
      }
    }

    it("should extract None if an optional attribute is not present") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr2" << "value")) ~> {
          session("attr" ?) { attr ⇒ complete(attr.toString) }
        } ~> check {
          responseAs[String] should be("None")
        }
      }
    }

    it("should extract None if the session cookie is not present") {
      new PlainBaker with CookieBuilding {
        Get() ~> {
          session("attr" ?) { attr ⇒ complete(attr.toString) }
        } ~> check {
          responseAs[String] should be("None")
        }
      }
    }

    it("should extract the default value if an optional attribute is not present") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr2" << "value")) ~> {
          session("attr" ? "default") { attr ⇒ complete(attr) }
        } ~> check {
          responseAs[String] should be("default")
        }
      }
    }

    it("should extract the default value if the session cookie is not present") {
      new PlainBaker with CookieBuilding {
        Get() ~> {
          session("attr" ? "default") { attr ⇒ complete(attr) }
        } ~> check {
          responseAs[String] should be("default")
        }
      }
    }
  }

  describe("The removeSession directive") {
    it("should remove session cookie") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr" << "value")) ~> {
          removeSession() { complete("ok") }
        } ~> check {
          header[`Set-Cookie`] match {
            case Some(`Set-Cookie`(HttpCookie(name, "deleted", _, _, _, _, _, _, _))) if name == baker.name ⇒
            case _ ⇒ fail
          }
        }
      }
    }
  }

  describe("The newSession directive") {
    it("should remove old session data and set new ones") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("attr" << "value")) ~> {
          newSession("attr" -> "newvalue") { complete("ok") }
        } ~> check {
          header[`Set-Cookie`] match {
            case Some(`Set-Cookie`(HttpCookie(name, "attr=newvalue", _, _, _, _, _, _, _))) if name == baker.name ⇒
            case _ ⇒ fail
          }
        }
      }
    }
  }

  describe("The setSession directive") {
    it("should append new values to existing ones") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("a1" << "v1")) ~> {
          setSession("a2" -> "v2") { complete("ok") }
        } ~> check {
          header[`Set-Cookie`] match {
            case Some(`Set-Cookie`(HttpCookie(name, "a1=v1&a2=v2", _, _, _, _, _, _, _))) if name == baker.name ⇒
            case Some(`Set-Cookie`(HttpCookie(name, "a2=v2&a1=v1", _, _, _, _, _, _, _))) if name == baker.name ⇒
            case _ ⇒ fail
          }
        }
      }
    }

    it("should remove specified attributes from the session") {
      new PlainBaker with CookieBuilding {
        Get() ~> addHeader(createCookie("a1" << "v1", "a2" << "v2")) ~> {
          setSession("a2" remove) { complete("ok") }
        } ~> check {
          header[`Set-Cookie`] match {
            case Some(`Set-Cookie`(HttpCookie(name, "a1=v1", _, _, _, _, _, _, _))) if name == baker.name ⇒
            case _ ⇒ fail
          }
        }
      }
    }
  }
}
