package binarycamp.spray.session

import scala.util.control.NonFatal
import spray.http.{ HttpCookie, HttpRequest }
import spray.http.HttpHeaders.Cookie
import spray.httpx.unmarshalling._
import spray.util._

trait CookieBaker {
  def coder: ContentCoder

  def name: String

  def maxAge: Option[Long]

  def domain: Option[String]

  def path: Option[String]

  def secure: Boolean

  def httpOnly: Boolean

  def encode(session: Session): Either[EncodingError, HttpCookie]

  def decode(cookie: HttpCookie): Either[DecodingError, Session]
}

object CookieBaker {
  implicit def default(implicit settings: SessionSettings): CookieBaker = {
    import settings._
    apply(ContentCoder(coderName, coderConfig), name, maxAge, domain, path, secure, httpOnly)
  }

  def apply(coder: ContentCoder, name: String, maxAge: Option[Long] = None, domain: Option[String] = None,
            path: Option[String] = None, secure: Boolean = false, httpOnly: Boolean = true): CookieBaker =
    new CookieBakerImpl(coder, name, maxAge, domain, path, secure, httpOnly)

  def attribute[T](request: HttpRequest, name: String)(implicit baker: CookieBaker,
                                                       deserializer: FromStringOptionDeserializer[T]): Option[T] =
    for {
      header ← request.headers.findByType[Cookie]
      cookie ← header.cookies.find(cookie ⇒ cookie.name == baker.name)
      session ← baker.decode(cookie).toOption
      attribute ← deserializer(session.get(name)).toOption
    } yield attribute
}

private class CookieBakerImpl(val coder: ContentCoder, val name: String, val maxAge: Option[Long],
                              val domain: Option[String], val path: Option[String], val secure: Boolean,
                              val httpOnly: Boolean) extends CookieBaker {
  override def encode(session: Session): Either[EncodingError, HttpCookie] =
    try {
      val content = coder.encode(session)
      Right(HttpCookie(name, content, maxAge = maxAge, domain = domain, path = path, secure = secure, httpOnly = httpOnly))
    } catch { case NonFatal(e) ⇒ Left(EncodingError(e.getMessage, e)) }

  override def decode(cookie: HttpCookie): Either[DecodingError, Session] =
    try Right(coder.decode(cookie.content))
    catch { case NonFatal(e) ⇒ Left(DecodingError(e.getMessage, e)) }
}
